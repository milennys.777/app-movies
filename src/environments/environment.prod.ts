export const environment = {
  production: true,
  url: 'https://api.themoviedb.org/3',
  //apiKey de moviedb
  apiKey: '',
  imgPath: 'https://image.tmdb.org/t/p'
};
